import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created: 12.03.2023
 *
 * @author Nico Haider (20200527)
 */
public class PathOfSurvival {
    public static void main(String[] args) {
        String filename = "src/main/resources/playerdata.bin";

        findPlayer(filename, "HunterKiller11111elf", 15000, 1337, 0);
        findPlayer(filename, "CyberBob", 2, 1, 354);
        findPlayer(filename, "ShadowDeath42", 3, 0, 400);

        List<Player> players = readPlayers(filename);
        if (players != null) {
            players.stream().sorted().forEach(System.out::println);
        }
    }

    public static List<Player> readPlayers(String filename) {
        try (RandomAccessFile file = new RandomAccessFile(filename, "r")) {
            List<Player> players = new ArrayList<>();
            short playerCount = skipMetadata(file);

            try {
                for (int i = 0; i < playerCount; i++) {
                    short playerDataLength = file.readShort();

                    String name = file.readUTF();
                    int year = file.readInt();
                    int month = file.readInt();
                    int day = file.readInt();

                    int scorePerMinute = file.readInt();
                    int wins = file.readInt();
                    int losses = file.readInt();

                    players.add(new Player(name, LocalDate.of(year, month, day),
                            scorePerMinute, wins, losses));
                }
                return players;
            } catch (EOFException e) {}
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("file not found: " + filename);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return null;
    }

    public static void findPlayer(String filename, String alias, int newScorePerMinute, int newWins, int newLosses) {
        try (RandomAccessFile file = new RandomAccessFile(filename, "rw")) {
            short playerCount = skipMetadata(file);

            try {
                for (int i = 0; i < playerCount; i++) {
                    short playerDataLength = file.readShort();
                    String name = file.readUTF();
                    if (name.equals(alias)) {
                        file.skipBytes(12);
                        file.writeInt(newScorePerMinute);
                        file.writeInt(newWins);
                        file.writeInt(newLosses);
                        return;
                    } else {
                        file.skipBytes(24);
                    }
                }
                throw new IllegalArgumentException("player not found");
            } catch (EOFException e) {}
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("file not found: " + filename);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    private static short skipMetadata(RandomAccessFile file) throws IOException {
        file.skipBytes(4);
        int offset = file.readInt();
        file.skipBytes(1337);
        short playerCount = file.readShort();
        file.skipBytes(offset);
        return playerCount;
    }
}
