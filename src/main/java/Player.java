import java.time.LocalDate;

/**
 * Created: 12.03.2023
 *
 * @author Nico Haider (20200527)
 */
public record Player(String name, LocalDate lastMatch, int scorePerMinute, int wins, int losses) implements Comparable<Player> {
    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", lastMatch=" + lastMatch +
                ", scorePerMinute=" + scorePerMinute +
                ", wins=" + wins +
                ", losses=" + losses +
                '}';
    }


    @Override
    public int compareTo(Player o) {
        return Integer.compare(this.scorePerMinute, o.scorePerMinute);
    }
}
